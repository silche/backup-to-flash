################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1310_LAUNCHXL_TIRTOS.cmd 

C_SRCS += \
../CC1310_LAUNCHXL.c \
../backup_to_flash.c \
../ccfg.c \
../main_tirtos.c \
../uartecho.c 

C_DEPS += \
./CC1310_LAUNCHXL.d \
./backup_to_flash.d \
./ccfg.d \
./main_tirtos.d \
./uartecho.d 

OBJS += \
./CC1310_LAUNCHXL.obj \
./backup_to_flash.obj \
./ccfg.obj \
./main_tirtos.obj \
./uartecho.obj 

OBJS__QUOTED += \
"CC1310_LAUNCHXL.obj" \
"backup_to_flash.obj" \
"ccfg.obj" \
"main_tirtos.obj" \
"uartecho.obj" 

C_DEPS__QUOTED += \
"CC1310_LAUNCHXL.d" \
"backup_to_flash.d" \
"ccfg.d" \
"main_tirtos.d" \
"uartecho.d" 

C_SRCS__QUOTED += \
"../CC1310_LAUNCHXL.c" \
"../backup_to_flash.c" \
"../ccfg.c" \
"../main_tirtos.c" \
"../uartecho.c" 


