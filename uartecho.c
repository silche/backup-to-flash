/*
 * Copyright (c) 2015-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== uartecho.c ========
 */
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Example/Board Header files */
#include "Board.h"
#include "backup_to_flash.h"
//#include "flash.h"

//UART
UART_Handle uart;
#define MAX_NUM_RX_BYTES    200   // Maximum RX bytes to receive in one go
#define MAX_NUM_TX_BYTES    1000   // Maximum TX bytes to send in one go
uint32_t wantedRxBytes = 16;        // Number of bytes received so far
uint8_t rxBuf[MAX_NUM_RX_BYTES];   // Receive buffer
uint8_t txBuf[MAX_NUM_TX_BYTES];   // Transmit buffer

/*
 * Some devices have a minimum FLASH write size of 4-bytes (1 word).  Trying
 * to write a non-multiple of 4 amount of data will fail.  This array is
 * rounded up (to next multiple of 4) to meet this requirement. Refer to NVS
 * driver documentation for more details.
 */

static uint8_t buffer[PAGE_SIZE];

typedef struct Data_Struct{
    int sensor_1;
    char sensor_2;
} Data_Struct;

static Data_Struct My_Data;

static void write_to_terminal(char *arg)
{
    UART_write(uart, arg, strlen(arg));
}

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    char input;
    //int rand_num = rand();
    //char print[400] = {0};
    uint8_t *to_print;
    UART_Params uartParams;

    Data_Struct *Data = &My_Data;

    /* Call driver init functions */
    GPIO_init();
    UART_init();

    //NVS_init();

    /* Configure the LED pin */
    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Turn on user LED */
    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);

    /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_TEXT;
    uartParams.readDataMode = UART_DATA_TEXT;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 115200;

    uart = UART_open(Board_UART0, &uartParams);

    if (uart == NULL) {
        /* UART_open() failed */
        while (1);
    }

    to_print = (uint8_t *) malloc(1000 * sizeof(uint8_t));
    UART_write(uart, "STARTED\r\n\n", strlen("STARTED\r\n\n"));

    //������������� NVS
    if(!Init_mem_driver())
        write_to_terminal("NVS_open() failed.\n");

    write_to_terminal(to_print);


/*
    memset(Data,0x00,sizeof(My_Data));

    Data->sensor_1 = 10;
    Data->sensor_2 = 15;

    sprintf(to_print, "\nWriting:\nsensor_1: %d\nsensor_2: %d\n\n", Data->sensor_1, Data->sensor_2);
    write_to_terminal(to_print);

    write_to_terminal("Saving data to flash...\n\n");

    memcpy(buffer,Data,sizeof(My_Data)); // ������ ������ �� ��������� �����

    // Write
    Backup_data(buffer,sizeof(My_Data) );
    memset(buffer,0x00,sizeof(buffer)); //������� ������



    memset(Data,0x00,sizeof(My_Data));

    Data->sensor_1 = 9;
    Data->sensor_2 = 12;

    sprintf(to_print, "\nWriting:\nsensor_1: %d\nsensor_2: %d\n\n", Data->sensor_1, Data->sensor_2);
    write_to_terminal(to_print);

    write_to_terminal("Saving data to flash...\n\n");

    memcpy(buffer,Data,sizeof(My_Data)); // ������ ������ �� ��������� �����

    // Write
    Backup_data(buffer,sizeof(My_Data) );
    memset(buffer,0x00,sizeof(buffer)); //������� ������

*/


    memset(to_print,0,sizeof(to_print));

    //readbuf  = (uint8_t *) malloc(MAX_NUM_RX_BYTES * sizeof(uint8_t));


    srand ( time(NULL) );

    // Loop forever echoing
    while (1) {
        //int rxBytes = UART_read(uart, rxBuf, wantedRxBytes);
        //UART_read(uart, *readbuf, sizeof(readbuf));
        UART_read(uart, &input, 1);
        //write_to_terminal("Input:\n");
        UART_write(uart, &input, 1);

        //if(strcmp ((char *) input, (char *) "w") == 0)
        if(input == 'w')
        {
            write_to_terminal("\nWriting\n");

            memset(Data,0x00,sizeof(My_Data));

            Data->sensor_1 = rand() % 10 + 1;
            Data->sensor_2 = rand() % 10 + 1;

            sprintf(to_print, "\nWriting:\nsensor_1: %d\nsensor_2: %d\n\n", Data->sensor_1, Data->sensor_2);
            write_to_terminal(to_print);

            write_to_terminal("Saving data to flash...\n\n");

            memcpy(buffer,Data,sizeof(My_Data)); // ������ ������ �� ��������� �����

            // Write
            Backup_data(buffer,sizeof(My_Data) );
            memset(buffer,0x00,sizeof(buffer)); //������� ������


        }
        else if(input == 'r')
        {
            //Read

            memset(buffer,0x00,sizeof(buffer)); //������� ������
            if(Get_last_data(buffer,sizeof(My_Data)))
                if(strlen(buffer) > 0)
                {
                    memset(Data,0x00,sizeof(My_Data)); // ��������������� ������� ���������
                    memcpy(Data,&buffer,sizeof(My_Data)); // ����������� ������ �� ������ � ���������
                }

            memset(buffer,0x00,sizeof(buffer)); //������� ������

            sprintf(to_print, "\nGet_last_data:\nsensor_1: %d\nsensor_2: %d\n\n", Data->sensor_1, Data->sensor_2);
            write_to_terminal(to_print);

            if(Get_previous_data(buffer,sizeof(My_Data)))
                if(strlen(buffer) > 0)
                {
                    memset(Data,0x00,sizeof(My_Data)); // ��������������� ������� ���������
                    memcpy(Data,&buffer,sizeof(My_Data)); // ����������� ������ �� ������ � ���������
                }

            memset(buffer,0x00,sizeof(buffer)); //������� ������

            sprintf(to_print, "\nGet_previous_data:\nsensor_1: %d\nsensor_2: %d\n\n", Data->sensor_1, Data->sensor_2);
            write_to_terminal(to_print);


        }
        else if(input == 'c')
        {
            // Write signature directly from flash to the console
            //sprintf(to_print, "%s\n", regionAttrs.regionBase);
            //write_to_terminal(to_print);

            Erase_data(SECTOR_ONE);
            Erase_data(SECTOR_TWO);
            write_to_terminal("Erasing flash...\n");

            // Erase the entire flash region

        }
        //write_to_terminal("Reset the device.");
        //write_to_terminal(HEADER);
    }


}

