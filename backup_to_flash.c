/*
 *  ======== backup_to_flash.c ========
 *
 *  Created on: 17 ���. 2018 �.
 *      Author: Silegonchet
 */

#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include <ti/drivers/NVS.h>

#include "Board.h"
#include "backup_to_flash.h"

#pragma DATA_ALIGN(buffer, 4)

//Buffer for saving/reading data from NVS
static uint8_t buffer[PAGE_SIZE];

static NVS_Handle nvsHandle;
static NVS_Attrs regionAttrs;
static NVS_Params nvsParams;

/*
 *  ======== ������������� NVS API =======
 */
bool Init_mem_driver(void)
{
    NVS_init();
    NVS_Params_init(&nvsParams);
    // Get a NVS handle to write and read.
    // Note!: we are using default parameters!
    nvsHandle = NVS_open(Board_NVS0, &nvsParams);

    if (nvsHandle == NULL)
        return false;

    NVS_getAttrs(nvsHandle, &regionAttrs);

    /* The device sector size is critical to the NVS driver, check if the configured value is correct! */
    if(regionAttrs.sectorSize == PAGE_SIZE) {
        return true;
        ///System_abort("Incorrect sector size!\n");
    }

    return false;
}

/*
 *  ======== ��������� ���������� ���������� ������ =======
 */
void Get_props(char *inf)
{
    sprintf(inf, "Region Base Address: %d\nSector Size: %d\nRegion Size: %d\n\n",
            regionAttrs.regionBase, regionAttrs.sectorSize, regionAttrs.regionSize);
}

/*
 *  ======== ����������� �������� � ���������� ������������ ������ =======
 */
static bool Determine_last_data_page(size_t sector, size_t size_buf)
{
    char ret;
    uint32_t size = (size_buf + 5) & 0xFFFFFFFFC;// = (sizeof(bool) + sizeof(*data_buf) + 4) & 0xFFFFFFFFC; // Make sure size is a multiple of 4
    memset(buffer,0x00,sizeof(buffer)); //������� ������
    NVS_read(nvsHandle, sector, buffer, size); //��������� ���������� � ���, � ����� �������� �������� ������ ������
    ret = buffer[size_buf+1];
    return ret;
}

/*
 *  ======== ������ ������ �� Flash ������ � ������ � ����� =======
 */
static bool Restore_data(uint8_t *data_buf, size_t size_buf, bool last_data)
{
    bool result = false;

    if(Determine_last_data_page(SECTOR_ONE, size_buf) == !last_data)
    {
        if(Determine_last_data_page(SECTOR_TWO, size_buf) == last_data)
        {
            memcpy(data_buf,buffer,size_buf);
            result = true;
        }
    }
    else
    {
        memcpy(data_buf,buffer,size_buf); //buffer+1 = buffer+sizeof(bool), size-1 = size-sizeof(bool)
        result = true;
    }
    return result;
}

/*
 *  ======== ������ ���������� ������ �� Flash ������ � ������ � ����� =======
 */
bool Get_previous_data(uint8_t *data_buf, size_t size_buf)
{
    return Restore_data(data_buf, size_buf, false);
}

/*
 *  ======== ������ ��������� ���������� ������ �� Flash ������ � ������ � ����� =======
 */
bool Get_last_data(uint8_t *data_buf, size_t size_buf)
{
    return Restore_data(data_buf, size_buf, true);
}

/*
 *  ======== �������� ����� ������� �� Flash ������ =======
 */
void Erase_data(size_t sector)
{
    // Erase the entire flash region
    NVS_erase(nvsHandle, sector, regionAttrs.sectorSize);
}

/*
 *  ======== ������ ������ (����������� �������) �� Flash ������ =======
 */
static bool Update_data_in_flash(uint8_t *data_buf, size_t size_buf, size_t sector_early_data, size_t sector_newest_data)
{
    int_fast16_t result = -1;
    //��������� �������� ������ ������ �� ������ ����������, ��� ������ ������ ������ 4,
    uint32_t size = (size_buf + 1 + 4) & 0xFFFFFFFFC; // ����� "1" - ������ ����� ���� boolean
    uint8_t buf_early_data[PAGE_SIZE];
    memcpy(buf_early_data,buffer,size);

    //���������� ������
    memset(buffer,0x00,sizeof(buffer)); //������� ������

    // ����������� ������ � ������� �� ��������� ����� �� ������
    memcpy(buffer+size_buf+1, true, 1); //������ � ����� �������� �������� ������
    memcpy(buffer, data_buf, size_buf); //buffer + 1 = buffer + sizeof(bool)

    result = NVS_write(nvsHandle,sector_newest_data,buffer,size,NVS_WRITE_ERASE | NVS_WRITE_POST_VERIFY); //� ���� �������� ���������� ����� ��������� ������ - ���� true

    if(result == 0)
    {
        result = -1;
        memcpy(buf_early_data+size_buf+1, false, 1);
        result = NVS_write(nvsHandle,sector_early_data,buf_early_data,size, NVS_WRITE_ERASE | NVS_WRITE_POST_VERIFY);
    }
    if(result == 0) return true; else return false;
}

/*
 *  ======== �������� ��������� ����� ������ �� Flash =======
 */
bool Backup_data(uint8_t *data_buf, size_t size_buf)
{
    int_fast16_t result;
    memset(buffer,0x00,size_buf); //������� ������

    if(Determine_last_data_page(SECTOR_ONE, size_buf) == false)
    {
        if(Determine_last_data_page(SECTOR_TWO, size_buf) == true) //���� ��������� ������ �������� � ������� 30, � �� 29
            result = Update_data_in_flash(data_buf,size_buf,SECTOR_TWO,SECTOR_ONE);
        else
            result = Update_data_in_flash(data_buf,size_buf,SECTOR_ONE,SECTOR_TWO);
    }
    else
        result = Update_data_in_flash(data_buf,size_buf,SECTOR_ONE,SECTOR_TWO);

    return (result == NVS_STATUS_SUCCESS) ? true : false;
}
