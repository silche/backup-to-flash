### IoT smart home device data backup
This software module allows you to save and restore the data of the structure, represented in the form of a buffer to write, in non-volatile memory (flash).

NVS API is used to work with non-volatile memory.

The NVS parameters are defined in the file CC1310_LAUNCHXL.c. By default, the following parameters are set: 0x1B000 is the base (starting) address (marked as NVS_REGIONS_BASE), and the size of the area (REGIONSIZE) allocated for data saving = 4 kB (=0x1000) * 4.

Since it is necessary to use pages 29 and 30 (indexes [0...31]), we use pages with the starting addresses 0x1C000 (0x1B000 + 0x1000) and 0x1D000, respectively, which fits into the NVS size.

The starting addresses of the pages are set with the offset of the NVS base address (since the NVS API methods accept the offset), i.e. for the 29th page the offset will be 0x1000. 